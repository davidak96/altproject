# Exam Explanations

## I created a vpc with two public and private subnets each
![vpc](network.png)

## I also created a nat gateway with an elastic ip, route tables, and an internet gateway to ensure that each subnet had access to the internet.

## I created a cluster with two worker nodes using "t3.large" instance types
![cluster](altcluster.png)

## I created different namespaces to group the different pods in the cluster.
![namespaces](namespaces.png)

## The target services launched using yaml and terraform scripts were picked by specific nginx ingresses
![service](services.png)
![ingress](ingresses.png)

## A route 53 zone and CNAME records were also created
![route53](route_53.png)

## Four https domains were created: voting app, sockshop microservices app, grafana and prometheus.
![voting](voting.png)
![sockshop](sock.png)
![prometheus](prometheus.png)
![grafana](grafana.png)
