terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

terraform {
  cloud {
    organization = "rigamortus"

    workspaces {
      name = "new-workspace"
    }
    token = var.token
  }
}

provider "aws" {
  region                   = "eu-west-2"
  shared_credentials_files = ["~/.aws/credentials"]
  # profile                  = "default"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}

provider "kubectl" {
  host                   = data.aws_eks_cluster.altcluster.endpoint
  token                  = data.aws_eks_cluster_auth.altcluster.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.altcluster.certificate_authority[0].data)
  # config_path            = "~/.kube/config"
  load_config_file = false
}

provider "helm" {
  kubernetes {
    # config_path            = "~/.kube/config"
    host                   = data.aws_eks_cluster.altcluster.endpoint
    token                  = data.aws_eks_cluster_auth.altcluster.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.altcluster.certificate_authority[0].data)
  }
}

provider "kubernetes" {
  host  = aws_eks_cluster.altcluster.endpoint
  token = data.aws_eks_cluster_auth.altcluster.token
  cluster_ca_certificate = base64decode(
  data.aws_eks_cluster.altcluster.certificate_authority[0].data)
}
