variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "vpc cidr"
}

variable "subnet_cidr" {
  type    = list(any)
  default = ["10.0.1.0/24", "10.0.3.0/24"]
}

variable "region" {
  default = "eu-west-2"
}

variable "availability_zone" {
  type        = list(any)
  default     = ["eu-west-2a", "eu-west-2b"]
  description = "my availability zones"
}

variable "subnet" {
  default = ["mysubnet1", "mysubnet2"]
}

variable "security_groups" {
  default = [80, 443]
}

variable "token" {
  default = ""
}
variable "pubsubnets_cidr" {
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24"]
  description = "description"
}

variable "igw" {
  default = "myinternetgateway"
}

variable "aws_access_key_id" {
  type        = string
  default     = ""
  description = "description"
}

variable "clustername" {
  type        = string
  default     = "altcluster"
  description = "description"
}

variable "altnode" {
  type        = string
  default     = "altnode"
  description = "description"
}

variable "aws_secret_access_key" {
  default = ""
}

variable "namespace" {
  type    = string
  default = "monitoring"
}

variable "nginx" {
  type    = string
  default = "ingress"
}

variable "domain" {
  type = map(string)
  default = {
    domain_name = "davidaltschool.me"
    subdomain1  = "voting.davidaltschool.me"
    subdomain2  = "sockshop.davidaltschool.me"
    subdomain3  = "prometheus.davidaltschool.me"
    subdomain4  = "grafana.davidaltschool.me"  
  }
}
