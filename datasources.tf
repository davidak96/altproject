data "kubectl_filename_list" "manifests" {
  pattern = "./manifests/*.yaml"
}

data "aws_eks_cluster_auth" "altcluster" {
  name = aws_eks_cluster.altcluster.name
}

data "aws_eks_cluster" "altcluster" {
  name = aws_eks_cluster.altcluster.name
}

data "local_file" "lb_hostname" {
  filename = "${path.module}/lb_hostname.txt"
  depends_on = [null_resource.lb_hostname]
}
