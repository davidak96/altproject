resource "kubectl_manifest" "test" {
  count     = length(data.kubectl_filename_list.manifests.matches)
  yaml_body = file(element(data.kubectl_filename_list.manifests.matches, count.index))
}

resource "kubernetes_namespace" "ingress" {
  metadata {
    name = var.nginx
  }
}

resource "helm_release" "kube-prometheus" {
  name       = "prometheus"
  namespace  = var.namespace
  version    = "45.7.1"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  timeout    = 2000
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = var.namespace
  }
}

resource "kubernetes_namespace" "sock-shop" {
  metadata {
    name = "sock-shop"
  }
}

resource "helm_release" "create_ingress_controller" {
  depends_on = [kubectl_manifest.test, kubernetes_namespace.ingress]
  name       = "nginx-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx/"
  chart      = "ingress-nginx"
  version    = "4.5.2"
  namespace  = var.nginx
  # set {
  #   name = "fullnameOverride"
  #   value = "try"
  # }
  # set {
  #   name = "controller.name"
  #   value = "whatever" 
  # }
}


resource "kubernetes_ingress_v1" "grafana" {
  depends_on = [
    helm_release.create_ingress_controller, kubectl_manifest.test, helm_release.kube-prometheus
  ]
  metadata {
    name      = "grafana"
    namespace = "monitoring"
    annotations = {
      "kubernetes.io/ingress.class"              = "nginx"
      "nginx.ingress.kubernetes.io/ssl-redirect" = "true"
      "cert-manager.io/cluster-issuer"           = "letsencrypt-prod"
    }
  }

  spec {
    rule {
      host = "grafana.davidaltschool.me"
      http {
        path {
          backend {
            service {
              name = "prometheus-grafana"
              port {
                number = 80
              }
            }
          }
        }
      }
    }
    tls {
      secret_name = "try2"
      hosts       = ["grafana.davidaltschool.me"] 
    }
  }
}

resource "kubernetes_ingress_v1" "prometheus" {
  depends_on = [
    helm_release.create_ingress_controller, kubectl_manifest.test, helm_release.kube-prometheus
  ]
  metadata {
    name      = "prometheus"
    namespace = "monitoring"
    annotations = {
      "kubernetes.io/ingress.class"              = "nginx"
      "nginx.ingress.kubernetes.io/ssl-redirect" = "true"
      "cert-manager.io/cluster-issuer"           = "letsencrypt-prod"
    }
  }

  spec {
    rule {
      host = "prometheus.davidaltschool.me"
      http {
        path {
          backend {
            service {
              name = "prometheus-kube-prometheus-prometheus"
              port {
                number = 9090
              }
            }
          }
        }
      }
    }
    tls {
      secret_name = "try"
      hosts       = ["prometheus.davidaltschool.me"] 
    }
  }
}


resource "null_resource" "lb_hostname" {
  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name '${var.clustername}' --region '${var.region}' && kubectl get svc -n '${(var.nginx)}' -o jsonpath='{.status.loadBalancer.ingress[*].hostname}' > ${path.module}/lb_hostname.txt"
  }
}

resource "aws_route53_zone" "main" {
  name = var.domain["domain_name"]
}

resource "aws_route53_record" "grafana" {
  zone_id = aws_route53_zone.main.zone_id
  name    = var.domain["subdomain4"]
  type    = "CNAME"
  records = [data.local_file.lb_hostname.content]
}

resource "aws_route53_record" "prometheus" {
  zone_id = aws_route53_zone.main.zone_id
  name    = var.domain["subdomain3"]
  type    = "CNAME"
  records = [data.local_file.lb_hostname.content]
}

resource "aws_route53_record" "sockshop" {
  zone_id = aws_route53_zone.main.zone_id
  name    = var.domain["subdomain2"]
  type    = "CNAME"
  records = [data.local_file.lb_hostname.content]
}

resource "aws_route53_record" "voting" {
  zone_id = aws_route53_zone.main.zone_id
  name    = var.domain["subdomain1"]
  type    = "CNAME"
  records = [data.local_file.lb_hostname.content]
}
